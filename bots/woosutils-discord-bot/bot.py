import disnake
from disnake.ext import commands
import config
import os
import asyncio

bot = commands.Bot()

@bot.event
async def on_ready():
    #await bot.change_presence(activity=disnake.Game(name="делаю сервер комфортным"))
    print("WOOS Util Bot information!")
    print(f'{bot.user} WOOS Utils (BETA) has been started!')
    print(f'Add by lin: https://discord.com/oauth2/authorize?client_id={bot.user.id}&permissions=8&scope=bot%20applications.commands')
    while True:
        await bot.change_presence(activity=disnake.Game(name="WOOS"), status=disnake.Status.idle)
        await asyncio.sleep(15)
        await bot.change_presence(activity=disnake.Game(name="делаю сервер комфортным"))
        await asyncio.sleep(15)
        
for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        bot.load_extension(f'cogs.{filename[:-3]}')

bot.run(config.TOKEN)
