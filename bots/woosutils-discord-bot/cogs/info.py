import disnake
from disnake.ext import commands

class Info(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.slash_command(description="Информация о сервере")
    async def about(self, inter):
        embed=disnake.Embed(title="О сервере", description="**Привет!** Мы WOOS (World Of OSes). Здесь мы обсуждаем новые и старые технологии!\n Мы можем помочь с разными категориями: **Операционные системы**, **радиотехника**, **пк и смартфоны** и все остальное!\nЕще публикуем новости о разном (новые версии чего-то, изменения и т.д.).\nЧувствуйте себя как дома, потому что тебе постарается помочь каждый и поддержит практически в любой ситуации!", color=0x2b2d31)
        embed.set_thumbnail(url=inter.guild.icon.url)
        await inter.send(embed=embed)
            
def setup(bot: commands.Bot):
    bot.add_cog(Info(bot))
    
